﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
/// <summary>
/// Handles the removal of platforms
/// </summary>
public class Platform : MonoBehaviour
{
    private bool remove;
    [SerializeField] private float platformSpeed;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        remove = false;
        platformSpeed = 0.05f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void FixedUpdate()
    {
        Vector3 temp = new Vector3(platformSpeed, 0, 0);
        this.transform.position -= temp;
    }

    /// <summary>
    /// Returns whether the platform is to be removed
    /// </summary>
    public bool GetRemoveStatus()
    {
        return remove;
    }

    public void SetRemoveStatus(bool newStatus)
    {
        remove = newStatus;
    }

    /// <summary>
    /// Updates the platform speed to the supplied value
    /// </summary>
    public void UpdateVelocity(float newSpeed)
    {
        platformSpeed = newSpeed;
    }

    public float GetVelocity()
    {
        return this.platformSpeed;
    }

    /// <summary>
    /// Handles platform removal when it goes off screen
    /// </summary>
    /// <param name="collision"></param>
    public void OnTriggerExit2D(Collider2D collision)
    {
        //removes platform once eraser has passed over it
        if(collision.gameObject.tag == "Eraser")
        {
            remove = true;
        }
    }
}
