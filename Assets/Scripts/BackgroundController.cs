﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the movement of the Scrolling background
/// </summary>
public class BackgroundController : MonoBehaviour
{
    /// <summary>
    /// The material for the games background
    /// </summary>
    private Material background;

    /// <summary>
    /// Movement Vector for the background, controls how fast it moves
    /// </summary>
    private Vector2 movement;

    /// <summary>
    /// Editable velocity on the XAxis for the background
    /// </summary>
    public float velocity;

    // Start is called before the first frame update
    void Start()
    {
        //get the Background material and generate movement vector
        background = GetComponent<Renderer>().material;
        movement = new Vector2(velocity, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //add smoothed movement to offset
        background.mainTextureOffset += movement * Time.deltaTime;
    }
}
