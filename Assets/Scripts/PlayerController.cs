﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles all the Player characters functions and interactions
/// </summary>
public class PlayerController : MonoBehaviour
{
    //Animator component of the Player
    private Animator playerAni;

    //RigidBody component of the player
    private Rigidbody2D playerRigid;

    /// <summary>
    /// The number of coins the player currently holds
    /// </summary>
    private int coinCount;

    /// <summary>
    /// Players set jump height
    /// </summary>
    private Vector2 jumpHeight = new Vector2(0, 4);
    private AudioSource[] playerAudio;
    private Boolean playerAlive;

    // Start is called before the first frame update
    void Start()
    {
        playerAni = GetComponent<Animator>();
        playerRigid = GetComponent<Rigidbody2D>();
        coinCount = 5;
        playerAudio = GetComponents<AudioSource>();
        playerAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        playerAni.SetFloat("VerticalSpeed", playerRigid.velocity.y);

        //kill player if they go off screen
        if (transform.position.x <= -5 || transform.position.y <= -5)
        {
            playerAlive = false;
        }
    }

    /// <summary>
    /// Returns the players current velocity on the Y Axis
    /// </summary>
    /// <returns></returns>
    public float GetVerticalVelocity()
    {
        return playerRigid.velocity.y;
    }

    /// <summary>
    /// Resets the players position to start, called when level is reset
    /// </summary>
    public void PlayerReset()
    {
        transform.position = new Vector2(0, 1);
        coinCount = 0;
        playerAlive = true;
    }

    /// <summary>
    /// When called, will add a force on the vertical axis to propel the player to a set height
    /// </summary>
    public void Jump()
    {
        playerRigid.AddForce(jumpHeight, ForceMode2D.Impulse);
        playerAudio[0].Play();
    }

    /// <summary>
    /// Returns the number of coins the player has
    /// </summary>
    /// <returns></returns>
    public int GetCoinCount()
    {
        return this.coinCount;
    }

    /// <summary>
    /// Returns a boolean indicating whether the player is alive or dead
    /// </summary>
    /// <returns></returns>
    public Boolean GetPlayerStatus()
    {
        return this.playerAlive;
    }

    /// <summary>
    /// Handles player collision with a variety of objects in the game world
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "DeathZone")
        {
            playerAlive = false;
        }
        else if (collision.gameObject.tag == "PickUp")
        {
            //Add coin to count and remove object from the world
            coinCount++;
            playerAudio[1].Play();
            collision.gameObject.SetActive(false);
        }

        //interaction with obstacles
        else if (collision.gameObject.tag == "Obstacle")
        {
            Debug.Log("Ouch");
            //Remove up to 5 coins from the player on collision
            if(coinCount > 5)
            {
                coinCount -= 5;
            }
            else
            {
                coinCount = 0;
            }
        }
    }
}
