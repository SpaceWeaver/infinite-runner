﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedCoin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //deactivate when offscreen
        if (transform.position.x <= -5 || transform.position.y <= -5)
        {
            gameObject.SetActive(false);
        }
    }
}
