﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    // Start is called before the first frame update
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        //add pools to dictionary
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            //instantiate objects in pool up to specified size
            for (int i = 0; i < pool.size; i++)
            {
                GameObject temp = Instantiate(pool.prefab);
                temp.SetActive(false);
                objectPool.Enqueue(temp);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    /// <summary>
    /// Grabs next object in queue from the pool corresponding to the tag 
    /// and spawns it into the world at the specified position
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="position"></param>
    public GameObject SpawnFromPool(string tag, Vector3 position)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("The Tag " + tag + " Doesnt Exist");
            return null;
        }
        else
        {
            //Grab object from queue and set position
            GameObject objToSpawn = poolDictionary[tag].Dequeue();
            objToSpawn.transform.position = position;
            objToSpawn.SetActive(true);
            poolDictionary[tag].Enqueue(objToSpawn);
            return objToSpawn;
        }
    }
}
