﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Handles the processing of inputs on the Main Menu Screen
/// </summary>
public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// When called, loads the game and begins play
    /// </summary>
    public void StartGame()
    {
        SceneManager.LoadScene("GameScreen");
    }

    /// <summary>
    /// Exits the user from the game
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }
}
