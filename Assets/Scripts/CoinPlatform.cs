﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
// This class inherits from the class Platform
public class CoinPlatform : Platform
{
    public GameObject[] platformCoins;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    void OnEnable()
    {
        foreach (GameObject coin in platformCoins)
        {
            coin.SetActive(true);
        }
    }
}
