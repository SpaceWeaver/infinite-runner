﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private string[] platformTags;
    [SerializeField] private ObjectPool platformPool;
    private float lastPlatformHeight;
    private List<GameObject> generatedPlatforms;

    // Start is called before the first frame update
    void Start()
    {
        lastPlatformHeight = 0;
        generatedPlatforms = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Generates two flat platforms for the start of a new level
    /// </summary>
    public void Regenerate()
    {
        generatedPlatforms.Clear();
        generatedPlatforms.Add(platformPool.SpawnFromPool("Flat", new Vector3(0, 0, -15)));
        generatedPlatforms.Add(platformPool.SpawnFromPool("Flat", new Vector3(15, 0, -15)));
    }

    /// <summary>
    /// Used to generate a new random platform 
    /// called when a platform is deleted from the world
    /// </summary>
    public void GeneratePlatform(float gapSize)
    { 
        //generate new platform height based on spawners current height
        float platformHeight = Random.Range(lastPlatformHeight -1.5f,lastPlatformHeight + 1.5f);

        //ensure platforms never spawn below or above the screen
        if(platformHeight < -3)
        {
            platformHeight = -2.5f;
        }
        else if(platformHeight > 4)
        {
            platformHeight = 4;
        }

        //Record the generated platforms height
        lastPlatformHeight = platformHeight;

        //Pick a random platform type
        int platformType = Random.Range(0, platformTags.Length);

        //Creates a platform ahead of the spawner object at the randomised height
        GameObject temp = platformPool.SpawnFromPool(platformTags[platformType],
            new Vector3(gameObject.transform.position.x + gapSize, platformHeight, -15));

        Debug.Log(temp.transform.position);

        //check if platform from pool is already part of List
        if (!generatedPlatforms.Contains(temp))
        {
            generatedPlatforms.Add(temp);
        }
    }

    /// <summary>
    /// Returns the current List of platforms generated
    /// </summary>
    /// <returns></returns>
    public List<GameObject> GetPlatforms()
    {
        return generatedPlatforms;
    }
}
