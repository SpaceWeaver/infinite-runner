﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Controls Primary game loop and associated functions
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private ObjectPool coinPool;

    /// <summary>
    /// Controller for the given player game object
    /// </summary>
    private PlayerController playerControl;

    /// <summary>
    /// Tracks the current time the user has survived
    /// </summary>
    private float playerTime;

    /// <summary>
    /// Tracks the users current collected coin values
    /// </summary>
    private int playerCoins;

    /// <summary>
    /// Stores the time display from the HUD
    /// </summary>
    [SerializeField] private GameObject timeText;

    /// <summary>
    /// The coin display from the HUD
    /// </summary>
    [SerializeField] private GameObject coinText;
    [SerializeField] private GameObject[] pauseMenu;
    [SerializeField] private GameObject[] gameOverMenu;

    /// <summary>
    /// Array Containing all platforms in the level
    /// </summary>
    private List<GameObject> platforms;
    private List<GameObject> droppedCoins;

    /// <summary>
    /// Script for procedural generation of the level 
    /// </summary>
    [SerializeField] private LevelGenerator levelGenerator;
    private float platformSpeed = 0.05f;
    private float speedUpTimer;
    private float platformGap;

    // Start is called before the first frame update
    void Start()
    {
        //Get controllers for player and level generator
        playerControl = player.GetComponent<PlayerController>();
        Restart();
    }

    // Update is called once per frame
    void Update()
    {
        //only allow player to effect game when unpaused
        if (Time.timeScale == 1)
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Space) && playerControl.GetVerticalVelocity() == 0)
            {
                playerControl.Jump();
            }
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
            Debug.Log("on phone, you shouldnt see this");

            //Jumps if tap wasnt on a ui element and player isnt already jumping
            if (Input.touchCount == 1 && playerControl.GetVerticalVelocity() == 0
                && EventSystem.current.currentSelectedGameObject == null)
            {
                playerControl.Jump();
            }
#endif
            //check the players still alive
            if (playerControl.GetPlayerStatus() == false)
            {
                ShowGameOver();
            }

            //manages the spawning and removal of platforms
            if (platforms != null)
            {
                PlatformHandling();
            }

            //Check if player has lost coins
            int OldCoins = playerCoins;
            if (playerControl.GetCoinCount() < OldCoins)
            {
                int difference = OldCoins - playerControl.GetCoinCount();
                DropCoins(difference);
            }
            playerCoins = playerControl.GetCoinCount();

            //incremement speed every 25 seconds
            if(speedUpTimer >= 25)
            {
                platformSpeed += 0.02f;
                Debug.Log("Platform Speed now: " + platformSpeed);
                speedUpTimer -= 25;
                IncreasePlatformGap();
            }

            //ensure platforms are moving at the same pace
            for (int i = 0; i < platforms.Count; i++)
            {
                Platform temp = platforms[i].GetComponent<Platform>();
                if(temp.GetVelocity() != platformSpeed)
                {
                    temp.UpdateVelocity(platformSpeed);
                }
            }

            //Update player time and coin count
            timeText.GetComponent<Text>().text = "Time: " + System.Math.Round(playerTime, 2);
            coinText.GetComponent<Text>().text = "Coins: " + playerCoins;
            speedUpTimer += Time.deltaTime;
            playerTime += Time.deltaTime;
        }
    }

    /// <summary>
    /// Allows the user to freeze the game,
    /// opens an overlay menu allowing the user to restart or quit
    /// </summary>
    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            ShowPauseMenu();
        }
        else
        {
            Time.timeScale = 1;
            HidePauseMenu();
        }
    }

    /// <summary>
    /// Displays the pause menu elements to the screen
    /// </summary>
    private void ShowPauseMenu()
    {
        foreach (GameObject i in pauseMenu)
        {
            i.SetActive(true);
        }
    }

    /// <summary>
    /// Hides the pause menu elements from the screen
    /// </summary>
    private void HidePauseMenu()
    {
        foreach (GameObject i in pauseMenu)
        {
            i.SetActive(false);
        }
    }

    /// <summary>
    /// Displays the Game Over screen to the player
    /// </summary>
    private void ShowGameOver()
    {
        //Pause background gameplay
        Time.timeScale = 0;

        //Display gameover elements to the Screen
        foreach (GameObject i in gameOverMenu)
        {
            //Update player distance and score
            if (i.name == "PlayerTime")
            {
                i.GetComponentInChildren<Text>().text = "Time(s): " + System.Math.Round(playerTime, 2);
            }
            else if (i.name == "PlayerCoin")
            {
                i.GetComponentInChildren<Text>().text = "Coins: " + playerCoins;
            }
            i.SetActive(true);
        }
    }

    /// <summary>
    /// Hides game over screen
    /// </summary>
    private void HideGameOver()
    {
        foreach (GameObject i in gameOverMenu)
        {
            i.SetActive(false);
        }
    }

    /// <summary>
    /// Restarts the game scene when called
    /// </summary>
    public void Restart()
    {
        //clear the game of existing platforms and coins
        if (platforms != null)
        {
            for (int i = 0; i < platforms.Count; i++)
            {
                platforms[i].GetComponent<Platform>().SetRemoveStatus(false);
                platforms[i].SetActive(false);
            }
        }
        if(droppedCoins != null)
        {
            foreach (GameObject i in droppedCoins)
            {
                i.SetActive(false);
            }
        }
        else
        {
            droppedCoins = new List<GameObject>();
        }
        
        //Reset player score and distance
        playerCoins = 0;
        playerTime = 0;
        platformSpeed = 0.05f;
        speedUpTimer = 0;
        platformGap = 3;

        //Reset player position and generate new map
        playerControl.PlayerReset();
        levelGenerator.Regenerate();

        //store all generated platforms in an array
        platforms = levelGenerator.GetPlatforms();

        HidePauseMenu();
        HideGameOver();

        //start game time
        Time.timeScale = 1;
    }

    /// <summary>
    /// Checks if any platforms are marked for deletion and if one is found, removes and replaces it
    /// </summary>
    private void PlatformHandling()
    {
        //Check for platforms marked for destruction
        for (int i = 0; i < platforms.Count; i++)
        {
            Platform temp = platforms[i].GetComponent<Platform>();
            if (temp.GetRemoveStatus() == true)
            {
                //remove and replace marked platforms
                platforms[i].SetActive(false);
                levelGenerator.GeneratePlatform(platformGap);
                temp.SetRemoveStatus(false);
            }
        }
        //retrieve updated platform list
        platforms = levelGenerator.GetPlatforms();
    }

    private void IncreasePlatformGap()
    {
        if (platformGap < 15)
        {
            platformGap++;
        }
    }

    /// <summary>
    /// On collision with an obstacle the player drops a specified number of coins
    /// </summary>
    /// <param name="CoinCount"></param>
    private void DropCoins(int CoinCount)
    {
        Debug.Log("Coins to drop: " + CoinCount);
        for (int i = 0; i < CoinCount; i++)
        {
            GameObject temp = coinPool.SpawnFromPool("Coin",
                new Vector2(player.transform.position.x - 3,
                player.transform.position.y + 1.5f));

            //Add dropped coin to list
            if (!droppedCoins.Contains(temp))
            {
                droppedCoins.Add(temp);
            }
        }
    }

    /// <summary>
    /// Returns the User to the Main Menu Scene
    /// </summary>
    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
